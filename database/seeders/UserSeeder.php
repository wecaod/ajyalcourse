<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
//        $prevUser=User::where('email','dev.abdallahalkahlout@gmail.com')->first();
//        if(!$prevUser){
//            $user=new User();
//            $user->name="Mousa";
//            $user->mobile="972597773989";
//            $user->email="dev.abdallahalkahlout@gmail.com";
//            $user->email_verified_at=Carbon::now();
//            $user->password=Hash::make('123456');
//            $user->remember_token= Str::random(10);
//            $user->save();
//        }


        User::updateOrCreate([
            'email'=>"dev.abdallahalkahlout@gmail.com"
        ],[
            "name"=>"Mousa",
            "mobile"=>"972597773989",
            "email"=>"dev.abdallahalkahlout@gmail.com",
            "email_verified_at"=>Carbon::now(),
            "password"=>Hash::make('123456'),
            "remember_token"=>Str::random(10),
        ]);
    }
}
