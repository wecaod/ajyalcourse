<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate([
            'email'=>"dev.abdallahalkahlout@gmail.com"
        ],[
            "name"=>"Mousa",
            "mobile"=>"972597773989",
            "email"=>"dev.abdallahalkahlout@gmail.com",
            "email_verified_at"=>Carbon::now(),
            "password"=>Hash::make('123456'),
            "remember_token"=>Str::random(10),
        ]);
    }
}
