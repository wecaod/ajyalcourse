@extends('admin.layouts.app')
@section('content')

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"> Posts Lists</h3>
            </div>

            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a class="btn btn-primary" href="{{route('admin.post.create')}}" >
                        Create new post
                    </a>
                    {{--                    <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span>--}}
                    {{--                    <span class="example-copy" data-toggle="tooltip" title="" data-original-title="Copy code"></span>--}}
                </div>
            </div>

        </div>
        <div class="card-body">

            @if(session()->has('success'))
                <div class="row">


                    <label class="alert alert-success">
                        {{session()->get('success')}}
                    </label>
                </div>
            @endif

            {{app()->getLocale()}}
            <div class="row">
                <form action="{{route('admin.post.index')}}">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" value="{{request('title')}}"/>

                        </div>

                        <div class="form-group">
                            <select name="category_id" class="form-control">
                                <option value=""> Choose category</option>
                                @foreach($categories as $category)
                                <option value="{{$category->id}}" {{ request('category_id')==$category->id?'selected':'' }}>
                                    {{$category->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-secondary">
                                Filter
                            </button>
                            <a href="{{route('admin.post.index')}}"  class="btn btn-bg-light">
                                Clear
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th colspan="2">{{__('admin.posts.title')}}</th>
                    <th >{{__('admin.posts.category')}}</th>
                    <th >{{__('admin.posts.created_at')}}</th>

                </tr>
                </thead>
                <tbody>
                @php
                 \Carbon\Carbon::setLocale('ar');
                 @endphp
                @foreach($posts as $key=>$post)
                    <tr>
                        <td> {{ $loop->iteration }}</td>
                        <td>
                            <a href="{{$post->main_image_url}}" target="_blank">
                                <img src="{{$post->main_image_url}}" alt="لا يوجد صورة" width="50"/>
                            </a>

                        </td>
                        <td> {{$post->title}}</td>

                        <td> {{$post->category->name}}</td>
                        <td>{{
    \Carbon\Carbon::parse($post->created_at)->diffForHumans(now())

}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>




        </div>
    </div>
@endsection
