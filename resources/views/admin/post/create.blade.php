@extends('admin.layouts.app')
@section('content')

    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Create new post
            </h3>
            <div class="card-toolbar">

            </div>
        </div>
        <!--begin::Form-->

        <div class="card-body">

            {{--                                @if($errors->any())--}}
            {{--                                    <label class="alert alert-danger">--}}
            {{--                                        <ul class="list">--}}


            {{--                                        @foreach($errors->all() as $error)--}}

            {{--                                            <li> {{$error}}</li>--}}
            {{--                                       @endforeach--}}
            {{--                                        </ul>--}}
            {{--                                    </label>--}}
            {{--                                @endif--}}

            <form method="POST" action="{{ route('admin.post.store') }}" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('title') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="name" autofocus>

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Description') }}</label>

                    <div class="col-md-6">
                        <textarea id="description" type="text" class="form-control summernote  @error('description') is-invalid @enderror" name="description" required autocomplete="name" autofocus rows="10">{{old('description')}}</textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Category') }}</label>

                    <div class="col-md-6">

                        <select name="category_id" class="form-control">
                            <option value="" selected>اختر تصنيف الخبر</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"  {{old('category_id') ==$category->id ?'selected':''}}>
                                    {{$category->name}}
                                </option>
                            @endforeach

                        </select>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Article image') }}</label>

                    <div class="col-md-6">
                       <input type="file" accept="image/*" name="image"/>
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>


                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">
                        Save
                    </button>
                    {{--                                <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                </div>
            </form>
            <!--end::Form-->
        </div>
@endsection
@push('js')
    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
                height:400
            });
        });
    </script>
@endpush
