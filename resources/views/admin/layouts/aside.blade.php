<!--begin::Aside-->
<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Brand-->
    <div class="brand flex-column-auto" id="kt_brand">
        <!--begin::Logo-->
        <a href="javascript:;" class="brand-logo">
            <img alt="Logo" src="{{asset('admin_assets/media/logos/logo-light.png')}}" />
        </a>
        <!--end::Logo-->
        <!--begin::Toggle-->
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
							<span class="svg-icon svg-icon svg-icon-xl">
								<!--begin::Svg Icon | path:s/media/svg/icons/Navigation/Angle-double-left.svg')}}-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon points="0 0 24 0 24 24 0 24" />
										<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
										<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
									</g>
								</svg>
                                <!--end::Svg Icon-->
							</span>
        </button>
        <!--end::Toolbar-->
    </div>
    <!--end::Brand-->
    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <!--begin::Menu Container-->
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <ul class="menu-nav">

                <li class="menu-item" aria-haspopup="true">
                    <a href="{{route('admin.category.index')}}" class="menu-link">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Manage categories</span>
                    </a>
                </li>


                <li class="menu-item" aria-haspopup="true">
                    <a href="{{route('admin.post.index')}}" class="menu-link">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Mange articles</span>
                    </a>
                </li>

                <li class="menu-item" aria-haspopup="true">
                    <a href="{{route('admin.users.index')}}" class="menu-link">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Mange Admins</span>
                    </a>
                </li>
                <li class="menu-item" aria-haspopup="true">
                    <a href="{{route('admin.notification.form')}}" class="menu-link">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Notifications </span>
                    </a>
                </li>
{{--                <li class="menu-section">--}}
{{--                    <h4 class="menu-text">Custom</h4>--}}
{{--                    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-web"></i>--}}
{{--                        <span class="menu-text">Applications</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Applications</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Users</span>--}}
{{--                                    <span class="menu-label">--}}
{{--														<span class="label label-rounded label-primary">6</span>--}}
{{--													</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/list-default" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Default</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/list-datatable" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Datatable</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/list-columns-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/list-columns-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/add-user" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Add User</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/user/edit-user" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Edit User</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Profile</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Profile 1</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/profile/profile-1/overview" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Overview</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/profile/profile-1/personal-information" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Personal Information</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/profile/profile-1/account-information" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Account Information</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/profile/profile-1/change-password" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Change Password</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/profile/profile-1/email-settings" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Email Settings</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/profile/profile-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Profile 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/profile/profile-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Profile 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/profile/profile-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Profile 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Contacts</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/contacts/list-columns" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/contacts/list-datatable" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Datatable</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/contacts/view-contact" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">View Contact</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/contacts/add-contact" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Add Contact</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/contacts/edit-contact" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Edit Contact</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Projects</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/list-columns-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/list-columns-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/list-columns-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/list-columns-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Columns 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/list-datatable" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">List - Datatable</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/view-project" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">View Project</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/add-project" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Add Project</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/projects/edit-project" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Edit Project</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Support Center</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/home-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Home 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/home-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Home 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/faq-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">FAQ 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/faq-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">FAQ 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/faq-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">FAQ 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/feedback" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Feedback</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/support-center/license" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">License</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Chat</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/chat/private" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Private</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/chat/group" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Group</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/chat/popup" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Popup</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Todo</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/todo/tasks" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Tasks</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/todo/docs" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Docs</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/todo/files" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Files</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Education</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">School</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/dashboard" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Dashboard</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/statistics" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Statistics</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/calendar" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Calendar</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/library" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Library</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/teachers" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Teachers</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/school/students" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Students</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Student</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/student/dashboard" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Dashboard</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/student/profile" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Profile</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/student/calendar" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Calendar</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/apps/education/student/classmates" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Classmates</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/education/class/dashboard" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Class</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">eCommerce</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/dashboard" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Dashboard 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/dashboard-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Dashboard 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/dashboard-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Dashboard 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/dashboard-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Dashboard 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/dashboard-5" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Dashboard 5</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/my-orders" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">My Orders</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/order-details" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Order Details</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/shopping-cart" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Shopping Cart</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/apps/ecommerce/checkout" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Checkout</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="custom/apps/inbox" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-line">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Inbox</span>--}}
{{--                                    <span class="menu-label">--}}
{{--														<span class="label label-danger label-inline">new</span>--}}
{{--													</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-tabs"></i>--}}
{{--                        <span class="menu-text">Pages</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Pages</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Login</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/login/login-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Login 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/login/login-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Login 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Login 3</span>--}}
{{--                                                <span class="menu-label">--}}
{{--																	<span class="label label-inline label-info">Wizard</span>--}}
{{--																</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-3/signup" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Sign Up</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-3/signin" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Sign In</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-3/forgot" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Forgot Password</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Login 4</span>--}}
{{--                                                <span class="menu-label">--}}
{{--																	<span class="label label-inline label-info">Wizard</span>--}}
{{--																</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-4/signup" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Sign Up</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-4/signin" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Sign In</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/login-4/forgot" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Forgot Password</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Classic</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-1" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 1</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-2" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 2</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-3" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 3</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-4" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 4</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-5" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 5</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="custom/pages/login/classic/login-6" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">Login 6</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Wizard</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-5" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 5</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/wizard/wizard-6" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Wizard 6</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Pricing Tables</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/pricing/pricing-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Pricing Tables 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/pricing/pricing-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Pricing Tables 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/pricing/pricing-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Pricing Tables 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/pricing/pricing-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Pricing Tables 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Invoices</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-5" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 5</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/invoices/invoice-6" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Invoice 6</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Error</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-1" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 1</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-3" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 3</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-4" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-5" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 5</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="custom/pages/error/error-6" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Error 6</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-section">--}}
{{--                    <h4 class="menu-text">Layout</h4>--}}
{{--                    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-graphic"></i>--}}
{{--                        <span class="menu-text">Themes</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Themes</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/themes/aside-light" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Light Aside</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/themes/header-dark" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Dark Header</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-layers"></i>--}}
{{--                        <span class="menu-text">Subheaders</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Subheaders</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/subheader/toolbar" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Toolbar Nav</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/subheader/actions" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Actions Buttons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/subheader/tabbed" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tabbed Nav</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/subheader/classic" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Classic</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/subheader/none" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">None</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu menu-item-open menu-item-here" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-interface-8"></i>--}}
{{--                        <span class="menu-text">General</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">General</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/general/fluid-content" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Fluid Content</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/general/minimized-aside" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Minimized Aside</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/general/no-aside" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">No Aside</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-active" aria-haspopup="true">--}}
{{--                                <a href="layout/general/empty-page" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Empty Page</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/general/fixed-footer" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Fixed Footer</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="layout/general/no-header-menu" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">No Header Menu</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item" aria-haspopup="true">--}}
{{--                    <a target="_blank" href="https://preview.keenthemes.com/metronic/demo1/builder" class="menu-link">--}}
{{--                        <i class="menu-icon flaticon-cogwheel-1"></i>--}}
{{--                        <span class="menu-text">Builder</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-section">--}}
{{--                    <h4 class="menu-text">CRUD</h4>--}}
{{--                    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-interface-7"></i>--}}
{{--                        <span class="menu-text">Forms</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Forms</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Form Controls</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/base" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Base Inputs</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/input-group" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Input Groups</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/checkbox" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Checkbox</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/radio" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Radio</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/switch" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Switch</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/controls/option" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Mega Options</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Form Widgets</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-datetimepicker" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Datetimepicker</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-datepicker" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Datepicker</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-timepicker" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Timepicker</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-daterangepicker" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Daterangepicker</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/tagify" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Tagify</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-touchspin" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Touchspin</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-maxlength" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Maxlength</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-switch" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Switch</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-multipleselectsplitter" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Multiple Select Splitter</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/bootstrap-select" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Bootstrap Select</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/select2" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Select2</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/typeahead" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Typeahead</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/nouislider" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">noUiSlider</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/form-repeater" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Form Repeater</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/ion-range-slider" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Ion Range Slider</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/input-mask" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Input Masks</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/jquery-mask" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">jQuery Mask</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/autosize" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Autosize</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/clipboard" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Clipboard</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/widgets/recaptcha" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Google reCaptcha</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Form Text Editors</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/editors/tinymce" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">TinyMCE</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                            <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">CKEditor</span>--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                            </a>--}}
{{--                                            <div class="menu-submenu">--}}
{{--                                                <i class="menu-arrow"></i>--}}
{{--                                                <ul class="menu-subnav">--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="crud/forms/editors/ckeditor-classic" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">CKEditor Classic</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="crud/forms/editors/ckeditor-inline" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">CKEditor Inline</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="crud/forms/editors/ckeditor-balloon" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">CKEditor Balloon</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="crud/forms/editors/ckeditor-balloon-block" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">CKEditor Balloon Block</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="menu-item" aria-haspopup="true">--}}
{{--                                                        <a href="crud/forms/editors/ckeditor-document" class="menu-link">--}}
{{--                                                            <i class="menu-bullet menu-bullet-line">--}}
{{--                                                                <span></span>--}}
{{--                                                            </i>--}}
{{--                                                            <span class="menu-text">CKEditor Document</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/editors/quill" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Quill Text Editor</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/editors/summernote" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Summernote WYSIWYG</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/editors/bootstrap-markdown" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Markdown Editor</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Form Layouts</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/layouts/default-forms" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Default Forms</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/layouts/multi-column-forms" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Multi Column Forms</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/layouts/action-bars" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Basic Action Bars</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/layouts/sticky-action-bar" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Sticky Action Bar</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Form Validation</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/validation/states" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Validation States</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/validation/form-controls" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Form Controls</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/forms/validation/form-widgets" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Form Widgets</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-tabs"></i>--}}
{{--                        <span class="menu-text">KTDatatable</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">KTDatatable</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Base</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/data-local" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Local Data</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/data-json" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">JSON Data</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/data-ajax" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Ajax Data</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/html-table" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">HTML Table</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/local-sort" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Local Sort</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/base/translation" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Translation</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Advanced</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/record-selection" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Record Selection</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/row-details" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Row Details</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/modal" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Modal Examples</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/column-rendering" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Column Rendering</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/column-width" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Column Width</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/advanced/vertical" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Vertical Scrolling</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Child Datatables</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/child/data-local" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Local Data</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/child/data-ajax" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Remote Data</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">API</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/api/methods" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">API Methods</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/ktdatatable/api/events" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Events</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-list-3"></i>--}}
{{--                        <span class="menu-text">Datatables.net</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Datatables.net</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Basic</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/basic/basic" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Basic Tables</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/basic/scrollable" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Scrollable Tables</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/basic/headers" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Complex Headers</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/basic/paginations" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Pagination Options</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Advanced</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/column-rendering" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Column Rendering</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/multiple-controls" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Multiple Controls</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/column-visibility" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Column Visibility</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/row-callback" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Row Callback</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/row-grouping" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Row Grouping</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/advanced/footer-callback" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Footer Callback</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Data sources</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/data-sources/html" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">HTML</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/data-sources/javascript" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Javascript</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/data-sources/ajax-client-side" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Ajax Client-side</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/data-sources/ajax-server-side" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Ajax Server-side</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Search Options</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/search-options/column-search" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Column Search</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/search-options/advanced-search" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Advanced Search</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Extensions</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/buttons" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Buttons</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/colreorder" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">ColReorder</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/keytable" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">KeyTable</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/responsive" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Responsive</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/rowgroup" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">RowGroup</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/rowreorder" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">RowReorder</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/scroller" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Scroller</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="crud/datatables/extensions/select" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">Select</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-upload-1"></i>--}}
{{--                        <span class="menu-text">File Upload</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="crud/file-upload/image-input" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Image Input</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="crud/file-upload/dropzonejs" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">DropzoneJS</span>--}}
{{--                                    <span class="menu-label">--}}
{{--														<span class="label label-danger label-inline">new</span>--}}
{{--													</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="crud/file-upload/uppy" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Uppy</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-section">--}}
{{--                    <h4 class="menu-text">Features</h4>--}}
{{--                    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-layers"></i>--}}
{{--                        <span class="menu-text">Bootstrap</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Bootstrap</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/typography" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Typography</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/buttons" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Buttons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/button-group" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Button Group</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/dropdown" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Dropdown</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/navs" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Navs</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/tables" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tables</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/progress" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Progress</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/modal" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Modal</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/alerts" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Alerts</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/popover" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Popover</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/bootstrap/tooltip" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tooltip</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-background"></i>--}}
{{--                        <span class="menu-text">Custom</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Custom</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/utilities" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Utilities</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/label" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Labels</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/pulse" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Pulse</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/line-tabs" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Line Tabs</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/advance-navs" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Advance Navs</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/timeline" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Timeline</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/pagination" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Pagination</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/symbol" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Symbol</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/overlay" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Overlay</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/spinners" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Spinners</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/iconbox" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Iconbox</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/callout" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Callout</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/ribbons" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Ribbons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/custom/accordions" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Accordions</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-interface-1"></i>--}}
{{--                        <span class="menu-text">Cards</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Cards</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/general" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">General Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/stacked" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Stacked Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/tabbed" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tabbed Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/draggable" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Draggable Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/tools" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Cards Tools</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/sticky" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Sticky Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/cards/stretched" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Stretched Cards</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-rocket"></i>--}}
{{--                        <span class="menu-text">Widgets</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Widgets</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/lists" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Lists</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/stats" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Stats</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/charts" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Charts</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/mixed" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Mixed</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/tiles" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tiles</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/engage" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Engage</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/base-tables" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Base Tables</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/advance-tables" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Advance Tables</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/feeds" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Feeds</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/widgets/nav-panels" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Nav Panels</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-open-box"></i>--}}
{{--                        <span class="menu-text">Icons</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/svg" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">SVG Icons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/custom-icons" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Custom Icons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/flaticon" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Flaticon</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/fontawesome5" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Fontawesome 5</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/lineawesome" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Lineawesome</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/icons/socicons" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Socicons</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-calendar"></i>--}}
{{--                        <span class="menu-text">Calendar</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Calendar</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/calendar/basic" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Basic Calendar</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/calendar/list-view" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">List Views</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/calendar/google" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Google Calendar</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/calendar/external-events" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">External Events</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/calendar/background-events" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Background Events</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-diagram"></i>--}}
{{--                        <span class="menu-text">Charts</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Charts</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/charts/apexcharts" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Apexcharts</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                                <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">amCharts</span>--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                </a>--}}
{{--                                <div class="menu-submenu">--}}
{{--                                    <i class="menu-arrow"></i>--}}
{{--                                    <ul class="menu-subnav">--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="features/charts/amcharts/charts" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">amCharts Charts</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="features/charts/amcharts/stock-charts" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">amCharts Stock Charts</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="menu-item" aria-haspopup="true">--}}
{{--                                            <a href="features/charts/amcharts/maps" class="menu-link">--}}
{{--                                                <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="menu-text">amCharts Maps</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/charts/flotcharts" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Flot Charts</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/charts/google-charts" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Google Charts</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-placeholder"></i>--}}
{{--                        <span class="menu-text">Maps</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Maps</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/maps/google-maps" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Google Maps</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/maps/leaflet" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Leaflet</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/maps/jqvmap" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">JQVMap</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">--}}
{{--                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                        <i class="menu-icon flaticon-light"></i>--}}
{{--                        <span class="menu-text">Miscellaneous</span>--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                    </a>--}}
{{--                    <div class="menu-submenu">--}}
{{--                        <i class="menu-arrow"></i>--}}
{{--                        <ul class="menu-subnav">--}}
{{--                            <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--												<span class="menu-link">--}}
{{--													<span class="menu-text">Miscellaneous</span>--}}
{{--												</span>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/kanban-board" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Kanban Board</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/sticky-panels" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Sticky Panels</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/blockui" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Block UI</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/perfect-scrollbar" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Perfect Scrollbar</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/treeview" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Tree View</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/bootstrap-notify" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Bootstrap Notify</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/toastr" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Toastr</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/sweetalert2" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">SweetAlert2</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/dual-listbox" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Dual Listbox</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/session-timeout" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Session Timeout</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/idle-timer" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Idle Timer</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="menu-item" aria-haspopup="true">--}}
{{--                                <a href="features/miscellaneous/cropper" class="menu-link">--}}
{{--                                    <i class="menu-bullet menu-bullet-dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="menu-text">Cropper</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
            </ul>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>
<!--end::Aside-->
