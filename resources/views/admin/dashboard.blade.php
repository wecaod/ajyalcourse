@extends('admin.layouts.app')
@section('content')

    <!--begin::Container-->
    <div class="container">
        <p>Blank page</p>


    </div>
    <!--end::Container-->

@endsection
@push('css')

@endpush
@push('js')
<script>
        $(document).ready(function(){
            messaging.requestPermission()
                .then(function () {
                    return messaging.getToken()
                })
                .then(function (token) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ route('admin.update.fcm.token') }}',
                        type: 'POST',
                        data: {
                            fcm_token: token,

                        },
                        dataType: 'JSON',
                        success: function (response) {
                            // console.log(response)
                        },
                        error: function (err) {
                            console.log(" Can't do because: " + err);
                        },
                    });
                })
        });

    </script>
@endpush
