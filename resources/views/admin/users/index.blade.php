@extends('admin.layouts.app')
@section('content')

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"> Admins Lists</h3>
            </div>

            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a class="btn btn-primary" href="{{route('admin.users.create')}}" >
                        Create new Admin
                    </a>
{{--                    <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span>--}}
{{--                    <span class="example-copy" data-toggle="tooltip" title="" data-original-title="Copy code"></span>--}}
                </div>
            </div>

        </div>
        <div class="card-body">

            @if(session()->has('success'))
                <div class="row">


                    <label class="alert alert-success">
                            {{session()->get('success')}}
                    </label>
                </div>
                @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>name</th>
                    <th>email</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td> {{$user->id}}</td>
                    <td> {{$user->name}}</td>
                    <td> {{$user->email}}</td>
                    <td> <a href="{{route('admin.users.edit',['admin'=>$user->id])}}" >
                            <i class="fa fa-pen"></i>
                        </a>
                    </td>

                    <td>




                        <form  action="{{ route('admin.users.destroy',$user->id) }} " method="POST"  class="delete-admin">
                            @csrf
                            <button class="btn btn-xs btn-danger" type="submit">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>

                </tr>
                @endforeach
                </tbody>
            </table>




        </div>
    </div>
@endsection
