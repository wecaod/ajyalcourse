@extends('admin.layouts.app')
@section('content')


                    <div class="card card-custom">
                        <div class="card-header">
                            <h3 class="card-title">
                              Create new Admin
                            </h3>
                            <div class="card-toolbar">

                            </div>
                        </div>
                        <!--begin::Form-->

                            <div class="card-body">

{{--                                @if($errors->any())--}}
{{--                                    <label class="alert alert-danger">--}}
{{--                                        <ul class="list">--}}


{{--                                        @foreach($errors->all() as $error)--}}

{{--                                            <li> {{$error}}</li>--}}
{{--                                       @endforeach--}}
{{--                                        </ul>--}}
{{--                                    </label>--}}
{{--                                @endif--}}

                                <form method="POST" action="{{ route('admin.users.store') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-2 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

{{--                                <div class="form-group mb-8">--}}
{{--                                    <div class="alert alert-custom alert-default" role="alert">--}}
{{--                                        <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>--}}
{{--                                        <div class="alert-text">--}}
{{--                                            The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mr-2">
                                    {{ __('Add new Admin') }}
                                </button>
{{--                                <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>




@endsection
