@extends('admin.layouts.app')
@section('content')


    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Create new Category
            </h3>
            <div class="card-toolbar">

            </div>
        </div>
        <!--begin::Form-->

        <div class="card-body">

            {{--                                @if($errors->any())--}}
            {{--                                    <label class="alert alert-danger">--}}
            {{--                                        <ul class="list">--}}


            {{--                                        @foreach($errors->all() as $error)--}}

            {{--                                            <li> {{$error}}</li>--}}
            {{--                                       @endforeach--}}
            {{--                                        </ul>--}}
            {{--                                    </label>--}}
            {{--                                @endif--}}

            <form method="POST" action="{{ route('admin.category.store') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>


                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">
                        Save
                    </button>
                    {{--                                <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                </div>
            </form>
            <!--end::Form-->
        </div>




@endsection
