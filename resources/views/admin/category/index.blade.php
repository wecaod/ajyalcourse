@extends('admin.layouts.app')
@section('content')

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"> Categories Lists</h3>
            </div>

            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a class="btn btn-primary" href="{{route('admin.category.create')}}" >
                        Create new Category
                    </a>
                    {{--                    <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span>--}}
                    {{--                    <span class="example-copy" data-toggle="tooltip" title="" data-original-title="Copy code"></span>--}}
                </div>
            </div>

        </div>
        <div class="card-body">

            @if(session()->has('success'))
                <div class="row">


                    <label class="alert alert-success">
                        {{session()->get('success')}}
                    </label>
                </div>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Number of posts</th>
                    <th>Created at</th>

                </tr>
                </thead>
                <tbody>
                @foreach($categories as $cat)
                    <tr>
                        <td> {{$cat->id}}</td>
                        <td> {{$cat->name}}</td>
                        <td> {{$cat->posts_count}}</td>
                        <td>{{
    \Carbon\Carbon::parse($cat->created_at)->format('Y-m-d');
}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>




        </div>
    </div>
@endsection
