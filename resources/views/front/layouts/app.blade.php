<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>World Time</title>
    <!-- plugin css for this page -->
    <link
        rel="stylesheet"
        href="{{asset('website_assets/vendors/mdi/css/materialdesignicons.min.css')}}"
    />
    <link rel="stylesheet" href=" {{asset('website_assets/vendors/aos/dist/aos.css/aos.css')}}" />

    <!-- End plugin css for this page -->
    <link rel="shortcut icon"   href=" {{asset('website_assets/images/favicon.png')}}" />

    <!-- inject:css -->
    @if(app()->getLocale()=="ar")
        <link rel="stylesheet"  href=" {{asset('website_assets/css/style.rtl.css')}}">
    @else
    <link rel="stylesheet"  href=" {{asset('website_assets/css/style.css')}}">
    @endif
    <!-- endinject -->
</head>

<body direction="{{app()->getLocale()=='ar'?'rtl':'ltr'}}" lang="{{app()->getLocale()}}">
<div class="container-scroller">
    <div class="main-panel">
        @include('front.layouts.header')

            @yield('content')
        <!-- main-panel ends -->
        <!-- container-scroller ends -->

        @include('front.layouts.footer')
    </div>
</div>
<!-- inject:js -->
<script src="{{asset('website_assets/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src=" {{asset('website_assets/vendors/aos/dist/aos.js/aos.js')}}"></script>
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="{{asset('website_assets/js/demo.js')}}"></script>
<script src="{{asset('website_assets/js/jquery.easeScroll.js')}}"></script>

<!-- End custom js for this page-->
</body>
</html>
