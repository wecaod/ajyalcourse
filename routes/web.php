<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(
    ['register'=>false]
);

Route::get('/',[\App\Http\Controllers\HomeController::class,'welcome'])->middleware('Localization');
Route::get('/post/{id}',[\App\Http\Controllers\HomeController::class,'viewPost'])->name('home.single.post');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');








Route::group(['middleware'=>['auth:web','Localization'],'prefix'=>'admin','as'=>'admin.'],function(){

Route::get('/', [App\Http\Controllers\AdminController::class, 'index'])->name('dashboard');
Route::post('/update-fcm-token', [App\Http\Controllers\AdminController::class, 'updateFcmToken'])->name('update.fcm.token');
Route::get('/locale/{lang}',[App\Http\Controllers\AdminController::class, 'changeLocale'])->name('change.locale');

        Route::group(["prefix"=>'users','as'=>'users.'],function(){
              Route::get('/', [App\Http\Controllers\Admin\UserConteoller::class, 'index'])->name('index');
              Route::get('/create', [App\Http\Controllers\Admin\UserConteoller::class, 'create'])->name('create');
              Route::post('/store', [App\Http\Controllers\Admin\UserConteoller::class, 'store'])->name('store');
              Route::get('/edit/{admin}', [App\Http\Controllers\Admin\UserConteoller::class, 'edit'])->name('edit');
              Route::post('/update/{admin}', [App\Http\Controllers\Admin\UserConteoller::class, 'update'])->name('update');
              Route::post('/destroy/{admin}', [App\Http\Controllers\Admin\UserConteoller::class, 'destroy'])->name('destroy');
        });

    Route::resource('category', \App\Http\Controllers\CategoryController::class);
    Route::resource('post', \App\Http\Controllers\PostController::class);
    Route::group(['prefix'=>'notifications'], function (){
        Route::get('/',[\App\Http\Controllers\Admin\NotificationController::class,'index'])->name('notification.form');
        Route::post('/send',[\App\Http\Controllers\Admin\NotificationController::class,'sendNotification'])->name('notification.send');
    });



});


//Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
