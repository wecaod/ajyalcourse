<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::group(['prefix'=>"articles"],function(){
    Route::get('/',[\App\Http\Controllers\Api\ArticleController::class,'index']);
    Route::get('/{articleId}',[\App\Http\Controllers\Api\ArticleController::class,'singleView']);
});
Route::group(['prefix'=>"auth"],function(){
        Route::post('/login',[\App\Http\Controllers\Api\AuthController::class,'login']);
        Route::post('/register',[\App\Http\Controllers\Api\AuthController::class,'register']);
        Route::group(['middleware'=>'auth:sanctum'],function(){
            Route::get('/logout',[\App\Http\Controllers\Api\AuthController::class,'logout']);
        });
});

Route::group(['middleware'=>"auth:sanctum"],function(){
    Route::get('/my-data',[\App\Http\Controllers\Api\AuthController::class,'MyData']);
});
