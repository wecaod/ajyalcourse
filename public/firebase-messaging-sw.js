/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
    apiKey: "AIzaSyBAynrsdIws_o70JxgADRXcsJ5mbUdnDB4",
    authDomain: "ajyalcourse.firebaseapp.com",
    projectId: "ajyalcourse",
    storageBucket: "ajyalcourse.appspot.com",
    messagingSenderId: "894212320473",
    appId: "1:894212320473:web:1cf17d475177d0667ca2c8",
    measurementId: "G-FKRB8JSQMB"

});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging2 = firebase.messaging();
messaging2.setBackgroundMessageHandler(function(payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    /* Customize notification here */
    const notificationTitle = "Background Message Title";
    const notificationOptions = {
        body: "Background Message body.",
        icon: "/itwonders-web-logo.png",
    };

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});
