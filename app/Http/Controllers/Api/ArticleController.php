<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticelResource;
use App\Models\Post;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use ApiResponse;

    public function index(){
        $articles=Post::orderBy('created_at','desc')->paginate(10);
        return $this->ApiResponse('تم العملية بنجاح','articles',ArticelResource::collection($articles),[
            'has_more_page'=>$articles->hasMorePages(),
            'main_page'=>true
        ]);
    }

    public function singleView(Request $request,Post $articleId){
        return $this->ApiResponse('تم العملية بنجاح','article',ArticelResource::make($articleId));
    }
}
