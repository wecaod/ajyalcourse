<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function welcome(){
        $data['posts']=Post::orderBy('created_at','desc')->take(2)->get();
        return view('website.index',$data);
    }

    public function viewPost(Post $id){
        $data['post']=$id;
        return view('website.post_single',$data);
    }
}
