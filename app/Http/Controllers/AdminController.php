<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(){
        return view('admin.dashboard');
    }

    public function changeLocale(Request $request ,$lang){
//        App::setLocale($lang);
        \app()->setLocale($lang);
        App::setLocale($lang);
//        return \app()->getLocale();
//        \app()->setLocale($lang);
        session(['locale'=>$lang]);
        return redirect()->back();
    }

    public function updateFcmToken(Request $request){

        $user=auth('web')->user();

        if($user){
            $user->fcm_token=$request->fcm_token;
            $user->save();
        }

        return response()->json(['message'=>'تم حفظ التوكن بنجاح']);
    }
}
