<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserConteoller extends Controller
{

    public function index(){

        $data['users']=User::get();
        return view('admin.users.index',$data);


    }

    public function create(){
        return view('admin.users.create');
    }


    public function store(Request $request){


        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]

        );
//        $validator=Validator::make($request->all(), [
//            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
//        ]);
//
        $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);



        if($user){
            return  redirect()->route('admin.users.index')
                ->with('success','تم انشاء المستخدم بنجاح ');
        }


//         return redirect()->with(['errors'=>$validator->errors])->route('admin.users.index');

    }

    public function edit( User $admin){
//            $admin = User::find($admin);
        $data['admin']=$admin;
        return view('admin.users.edit',$data);

    }
    public function update(Request $request,User $admin){
        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($admin->id)],

//                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]

        );

        $admin->update([
            'name' => $request->name,
            'email' => $request->email,
//            'password' => Hash::make($request->password),
        ]);
        if($admin){
            return  redirect()->route('admin.users.index')
                ->with('success','تم تحديث بيانات الآدمن بنجاح ');
        }

    }

    public function destroy(User $admin){
       if($admin->delete()) {
           return redirect()->route('admin.users.index')->with(['success'=>'تم حذف المستخدم بنجاح']);
       }
    }
}
