<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\NormalNotification;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    use ApiResponse;
    public  function  index(){
        $data['users']=User::whereNotNull('fcm_token')->get();
        return view('admin.notification.index',$data);
    }

    public function sendNotification(Request $request){

        $request->validate([
            'title'=>'required',
            'message'=>'required',
            'user_id'=>'required'
        ]);
        $user = User::find($request->user_id);

        if($user && $user->fcm_token){
            $message=[
                'title'=>$request->title,
                "message"=>$request->message,
                'web_link'=>route('admin.users.index')
            ];
            $this->sendFCM($user->fcm_token,$message);
            $user->notify(new NormalNotification($message));
        }

        return redirect()->back()->with(['success','تم ارسال الاشعار بنجاح']);
    }
}
