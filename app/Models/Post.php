<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable=['title','description','category_id','image'];
    protected $appends=['main_image_url'];

    public function category(){
        return $this->belongsTo(Category::class,'category_id')->withDefault(['name'=>'غير مصنف']);
    }
    public function getMainImageUrlAttribute(){
        if($this->image){

        return  asset('uploads/'.$this->image);
        }
        return null;
    }

    public function scopeSearch($q,$request){
        if($request->has('title') && $request->title){
            $q->where('title','like','%'.$request->title.'%');
        }
        if($request->has('category_id')&& $request->category_id){
            $q->where('category_id',$request->category_id);
        }
    }
}
