<?php
namespace App\Traits;



Trait ApiResponse{

    public function ApiResponse($message,$datakey,$data,$extraData=[],$errorsArray=[],$success=true,$statusCode=200){

        $Apidata=[
            'success'=>$success,
            'message'=>$message,
            "$datakey"=>$data
        ];
        foreach($extraData as $key=>$data){
            $Apidata[$key]=$data;
        }

        $errors = [];
        if(count($errorsArray)){
            foreach ($errorsArray as $key => $value) {
                if($key == 'email')
                    $key = 'message';
//                $errors[$key]=$valuee = is_array($value) ? implode(',', $value) : $value;
                $valuee = is_array($value) ? implode(',', $value) : $value;
                array_push($errors,$valuee);
                //implode is for when you have multiple errors for a same key
                //like email should valid as well as unique
            }
        }

        $Apidata['errors'] = $errors;



//            $Apidata->merge($extraData);

        return response()->json($Apidata,$statusCode);
    }

    function sendFCM($token, $message)
    {

        $fcm_server_key = "AAAA0DM1LNk:APA91bEh8GUnJBRx3jnKLwDi0Og6ii5-NA8PtgSpFzJi87StJp4ykUzq2QhuBGhEtR_5xM2R9TGQvW2oYj8oYFPI_R9ZyYpiO7uelBQUZZpiXqRokw6cE6F29_3xI25_YQamZaCmdWoS";
        $fcm_server_id = "894212320473";
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';


        $notification = [
            'title' => $message['title'],
            'body' =>$message['message'],
            "data" => $message,
            'icon' => 'myIcon',
            'sound' => 'mySound',
            // "click_action" => isset($message['web_url'])?$message['web_url']:isset($message['action'])?$message['action']:'',
            "click_action" => isset($message['web_url'])?$message['web_url']:'',
        ];
        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
        $fcmNotification = [
//            'registration_ids' => $this->tokens, //multple token array
            'to' => $token,
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers = array(
            'Authorization:key=' . $fcm_server_key,
            'sender:id=' . $fcm_server_id,
            'Content-Type:application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        // Execute post
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);
        return $result;
    }
}
